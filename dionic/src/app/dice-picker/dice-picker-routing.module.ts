import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DicePickerPage } from './dice-picker.page';

const routes: Routes = [
  {
    path: '',
    component: DicePickerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DicePickerPageRoutingModule {}
