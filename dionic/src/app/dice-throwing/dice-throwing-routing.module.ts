import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiceThrowingPage } from './dice-throwing.page';

const routes: Routes = [
  {
    path: '',
    component: DiceThrowingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiceThrowingPageRoutingModule {}
