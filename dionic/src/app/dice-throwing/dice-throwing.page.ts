import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import * as diceGlobals from '../dice';

@Component({
  selector: 'app-dice-throwing',
  templateUrl: './dice-throwing.page.html',
  styleUrls: ['./dice-throwing.page.scss'],
})
export class DiceThrowingPage implements OnInit {
	lastroll: number;

	constructor() {
		this.lastroll = undefined;
	}

	ngOnInit() {}

	throwDice() {
		this.lastroll = diceGlobals.globalDice.getRandomValueFromDice();
	}

}
