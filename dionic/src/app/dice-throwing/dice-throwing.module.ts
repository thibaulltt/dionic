import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiceThrowingPageRoutingModule } from './dice-throwing-routing.module';

import { DiceThrowingPage } from './dice-throwing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiceThrowingPageRoutingModule
  ],
  declarations: [DiceThrowingPage]
})
export class DiceThrowingPageModule {}
