import { Component, OnInit } from '@angular/core';
import * as diceGlobals from '../dice';

@Component({
  selector: 'app-dice-history',
  templateUrl: './dice-history.page.html',
  styleUrls: ['./dice-history.page.scss'],
})
export class DiceHistoryPage implements OnInit {
	d: diceGlobals.Dice;
  constructor() {
	  this.d = new diceGlobals.Dice();
  }

  ngOnInit() {
	  //
  }

	getGlobals() {
		return diceGlobals.globalDice.historyDice;
	}

	resetHistory() {
		diceGlobals.globalDice.historyDice = [];
	}
}
