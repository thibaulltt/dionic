import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiceHistoryPageRoutingModule } from './dice-history-routing.module';

import { DiceHistoryPage } from './dice-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiceHistoryPageRoutingModule
  ],
  declarations: [DiceHistoryPage]
})
export class DiceHistoryPageModule {}
