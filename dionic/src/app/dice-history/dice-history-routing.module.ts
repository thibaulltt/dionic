import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { Dice } from '../dice';

import { DiceHistoryPage } from './dice-history.page';

const routes: Routes = [
  {
    path: '',
    component: DiceHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), IonicModule],
  exports: [RouterModule],
})
export class DiceHistoryPageRoutingModule {}
